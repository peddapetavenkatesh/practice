﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DAL.Repositories
{
    public class DapperDbContext
    {
        private readonly string ConnectionString;
        public DapperDbContext(IConfiguration configuration)
        {
            ConnectionString =  configuration.GetConnectionString("DefaultConnection");
        }
        public IDbConnection connection { get => new SqlConnection(ConnectionString);}
    }
}
