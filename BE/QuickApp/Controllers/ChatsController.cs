﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.Models;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using QuickApp.Helpers;
using Microsoft.AspNet.SignalR;
using QuickApp.ViewModels;
using IdentityServer4.Extensions;
using DAL.Repositories;
using Dapper;

namespace QuickApp.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ChatsController : ControllerBase
    {
        
        private readonly ApplicationDbContext _context;
        private readonly ChatHub chatHub;
        private readonly DapperDbContext dapperDbContext;

        public ChatsController(ApplicationDbContext context, ChatHub chatHub, DapperDbContext dapperDbContext)
        {
            _context = context;
            this.chatHub = chatHub;
            this.dapperDbContext = dapperDbContext;
        }

        // GET: api/Chats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Chat>>> GetChats()
        {
            var data = new List<ChatViewModel.Get>();
            var query = $"select FromId from Chats where ToId = '{HttpContext.User.GetSubjectId()}' group by FromId order by Max(CreatedDate) desc;";
            var chatUsers = await dapperDbContext.connection.QueryAsync<string>(query);
            foreach (var item in chatUsers)
            {
                var user = new ChatViewModel.Get();
                
                var count = await _context.Chats.Where(s => s.IsSeen == false && s.FromId == item && s.ToId == HttpContext.User.GetSubjectId()).CountAsync();
                user.UserName = (await _context.Users.FindAsync(item)).UserName;
                user.NewMessageCount = count;
                user.isSeen = count > 0;
                foreach (var messagedUser in await _context.Chats.Where(s => s.FromId == item).OrderByDescending(s => s.CreatedDate).ToListAsync())
                {
                    user.messages.Add(new ChatViewModel.Get.UserMessage
                    {
                        Message = messagedUser.Message,
                        SendDate = messagedUser.CreatedDate
                    });
                }
                data.Add(user);
            }

            return Ok(data);
        }

        // GET: api/Chats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Chat>> GetChat(int id)
        {
            var chat = await _context.Chats.FindAsync(id);

            if (chat == null)
            {
                return NotFound();
            }

            return chat;
        }

        // PUT: api/Chats/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChat(int id, Chat chat)
        {
            if (id != chat.ChatId)
            {
                return BadRequest();
            }

            _context.Entry(chat).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Chats
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //// more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPost]
        //public async Task<ActionResult<Chat>> PostChat(Chat chat)
        //{
            
        //    _context.Chats.Add(chat);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetChat", new { id = chat.ChatId }, chat);
        //}

        // DELETE: api/Chats/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Chat>> DeleteChat(int id)
        {
            var chat = await _context.Chats.FindAsync(id);
            if (chat == null)
            {
                return NotFound();
            }

            _context.Chats.Remove(chat);
            await _context.SaveChangesAsync();

            return chat;
        }
        [HttpPost]
        public async Task<IActionResult> SendMessage([FromBody] ChatViewModel.Post body) 
        {
            var toUser = await _context.Users.FindAsync(body.ToUserId);
            await chatHub.SendMessageToUser(toUser.ConnectionId, body.Message);
            return Ok(new { message = "message sent"});
        }
        private bool ChatExists(int id)
        {
            return _context.Chats.Any(e => e.ChatId == id);
        }
    }
}
