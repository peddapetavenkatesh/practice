﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        [HttpGet]
        public IActionResult GetSome() 
        {
            return Ok(new { message = "sucesss", data = "hello world"});
        }
        [HttpPost("body")]
        public IActionResult testPost([FromBody] Test test) 
        {
            return Ok(test);
        }
        [HttpPost("form")]
        public IActionResult testform([FromForm] Test test)
        {
            return Ok(test);
        }

    }
    public class Test 
    {
        public string name { get; set; }
        public string email { get; set; }
    }
}
