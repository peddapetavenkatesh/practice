﻿using DAL;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.Helpers
{
    [EnableCors("CorsPolicy")]

    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ChatHub : Hub
    {
        private readonly ApplicationDbContext _context;

        public ChatHub(ApplicationDbContext _context)
        {
            this._context = _context;
        }
        public async Task SendMessageToUser(string ConnectionId, string message)
        {
            var toUser = await _context.Users.SingleOrDefaultAsync(s => s.ConnectionId == ConnectionId);
            await _context.Chats.AddAsync(new DAL.Models.Chat()
            {
                IsSeen = false,
                Message = message,
                ToId = toUser.Id,
                FromId = Context.User.GetSubjectId(),
            });
            await _context.SaveChangesAsync();
            var chatCount = await _context.Chats.Where(s => s.ToId == Context.User.GetSubjectId() && s.IsSeen == false).CountAsync();
            
            await Clients.Client(ConnectionId).SendAsync("MESSAGE_COUNT", chatCount);
        }
        public async override  Task OnConnectedAsync()
        {
            var user = await _context.Users.SingleOrDefaultAsync(s => s.Id == Context.User.GetSubjectId());
            user.ConnectionId = Context.ConnectionId;
            user.isOnline = true;
            user.LastLogin = DateTime.Now;
            await _context.SaveChangesAsync();
            var chatCount = await _context.Chats.Where(s => s.ToId == Context.User.GetSubjectId() && s.IsSeen == false).CountAsync();
            await Clients.Client(user.ConnectionId).SendAsync("MESSAGE_COUNT", chatCount);

            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            var user = await _context.Users.SingleOrDefaultAsync(s => s.Id == Context.User.GetSubjectId());
            user.ConnectionId = Context.ConnectionId;
            user.isOnline = false;
            user.LastLogin = DateTime.Now;
            await _context.SaveChangesAsync();

            await base.OnDisconnectedAsync(exception);
        }
    }
}
