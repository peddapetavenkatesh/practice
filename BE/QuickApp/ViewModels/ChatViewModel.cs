﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class ChatViewModel
    {
        public class Post 
        {
            public string ToUserId { get; set; }
            public string Message { get; set; }
        }
        public class Get 
        {
            public Get()
            {
                messages = new List<UserMessage>();
            }
            public string UserName { get; set; }
            public bool isSeen { get; set; }
            public int NewMessageCount { get; set; }
            public List<UserMessage> messages{ get; set; }
            public class UserMessage 
            {
                public string Message { get; set; }
                public DateTime SendDate { get; set; }
            }
        }
        
    }

}
