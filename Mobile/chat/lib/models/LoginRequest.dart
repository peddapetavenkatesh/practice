// To parse this JSON data, do
//
//     final loginRequest = loginRequestFromJson(jsonString);

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) =>
    LoginRequest.fromJson(json.decode(str));

String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
  LoginRequest({
    this.clientId,
    this.scopes,
    this.grantType,
    this.username,
    this.password,
  });

  String clientId;
  String scopes;
  String grantType;
  String username;
  String password;

  factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
        clientId: json["client_Id"] == null ? null : json["client_Id"],
        scopes: json["scopes"] == null ? null : json["scopes"],
        grantType: json["grant_type"] == null ? null : json["grant_type"],
        username: json["username"] == null ? null : json["username"],
        password: json["password"] == null ? null : json["password"],
      );

  Map<String, dynamic> toJson() => {
        "client_Id": clientId == null ? null : clientId,
        "scopes": scopes == null ? null : scopes,
        "grant_type": grantType == null ? null : grantType,
        "username": username == null ? null : username,
        "password": password == null ? null : password,
      };
}
