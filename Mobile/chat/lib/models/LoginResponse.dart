// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.accessToken,
    this.expiresIn,
    this.tokenType,
    this.refreshToken,
    this.scope,
  });

  String accessToken;
  int expiresIn;
  String tokenType;
  String refreshToken;
  String scope;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        accessToken: json["access_token"] == null ? null : json["access_token"],
        expiresIn: json["expires_in"] == null ? null : json["expires_in"],
        tokenType: json["token_type"] == null ? null : json["token_type"],
        refreshToken:
            json["refresh_token"] == null ? null : json["refresh_token"],
        scope: json["scope"] == null ? null : json["scope"],
      );

  Map<String, dynamic> toJson() => {
        "access_token": accessToken == null ? null : accessToken,
        "expires_in": expiresIn == null ? null : expiresIn,
        "token_type": tokenType == null ? null : tokenType,
        "refresh_token": refreshToken == null ? null : refreshToken,
        "scope": scope == null ? null : scope,
      };
}
