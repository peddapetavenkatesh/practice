import 'dart:collection';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:chat/models/LoginRequest.dart';
import 'package:chat/services/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';

import 'ForgetPassword.dart';
import 'MainPage.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isChecked = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController pswController = TextEditingController();
  TextEditingController usrController = TextEditingController();

  bool _autoValidate;

  bool _password = true;
  @override
  void initState() {
    super.initState();
    _autoValidate = false;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        //  resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
          child: Container(
//         decoration: BoxDecoration(
//            image: DecorationImage(
//              image: AssetImage("assets/images/Back1.jpg"),
//              fit: BoxFit.cover,
//            )
//          ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * .94,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: MediaQuery.of(context).size.width / 8
//                height: 100,
                          ),
                      Container(
//                  height: 120,
//                  width: 120,
                        height: MediaQuery.of(context).size.width / 4,
                        width: MediaQuery.of(context).size.height / 4,

                        child: Center(
                            child: Image.network(
                          "https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80%20334w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80%20634w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=668&q=80%20668w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=934&q=80%20934w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1234&q=80%201234w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1268&q=80%201268w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1534&q=80%201534w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1834&q=80%201834w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1868&q=80%201868w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2134&q=80%202134w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2434&q=80%202434w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2468&q=80%202468w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2734&q=80%202734w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3034&q=80%203034w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3068&q=80%203068w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3334&q=80%203334w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3634&q=80%203634w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3668&q=80%203668w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3934&q=80%203934w,%20https://images.unsplash.com/photo-1544164560-c70dbbe073cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=4000&q=80%204000w",
                          fit: BoxFit.cover,
                        )),
                      ),
//                      CircleAvatar(
//                        radius: 55,
//                        backgroundImage: AssetImage('assets/images/Text.jpg'),
//                      ),
                      Text(
                        "Merchant ",
                        style: TextStyle(
                            color: Colors.red[900],
                            fontSize: 30,
                            fontWeight: FontWeight.w800,
                            fontStyle: FontStyle.italic),
                      ),
                      SizedBox(
//                           height: 50,
                        height: MediaQuery.of(context).size.width / 13,
                      ),
                      Container(
//                  height: 250,
                        height: MediaQuery.of(context).size.height / 3,
                        child: Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Sign In",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 25.0),
                              ),
                              ListTile(
                                title: TextFormField(
                                  textInputAction: TextInputAction.go,
                                  validator: emailValidator,
                                  controller: usrController,
                                  decoration: InputDecoration(
                                    labelText: 'User Name',
                                    prefixIcon: const Icon(
                                      Icons.account_circle,
                                    ),
                                  ),
                                ),
                              ),
                              ListTile(
//                                leading: const Icon(Icons.person),
                                title: TextFormField(
                                  textInputAction: TextInputAction.go,
                                  obscureText: _password,
                                  validator: pswValidator,
                                  controller: pswController,
                                  decoration: InputDecoration(
                                    labelText: 'Password',
                                    prefixIcon: const Icon(
                                      Icons.lock,
                                    ),
                                    suffixIcon: GestureDetector(
                                      child: _password
                                          ? Icon(Icons.remove_red_eye,
                                              color: Colors.black)
                                          : Icon(Icons.remove_red_eye,
                                              color: Colors.red),
                                      onTap: () {
                                        setState(() {
                                          _password = !_password;
                                        });
                                      },
//                              onTap: _toggle,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      InkWell(
                        onTap: () {
                          _validateInputs();
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.width / 10,
                          width: MediaQuery.of(context).size.width * .6,
                          child: Center(
                              child: Text(
                            "Login",
                            style: TextStyle(
                                fontWeight: FontWeight.w800,
                                color: Colors.white,
                                fontSize: 18),
                          )),
                          decoration: BoxDecoration(
                            color: Colors.blue[900],
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: Colors.black),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.width / 10.0,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ForgetPassword()));
                        },
                        child: Text(
                          "Forgot Password ?",
                          style: TextStyle(
                              color: Colors.blue,
                              fontStyle: FontStyle.italic,
                              decorationStyle: TextDecorationStyle.solid),
                        ),
                      ),

                      SizedBox(
//                height: 30,
                        height: MediaQuery.of(context).size.width / 10.5,
                      ),
                    ],
                  ),
                  Positioned(
                    bottom: 14,
                    left: 1,
                    right: 1,
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Powered By',
                          style: TextStyle(
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w800),
                        ),
                        Text(
                          'CHS Software Solutions Pvt.Ltd.',
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      SharedPreferences _prefs = await SharedPreferences.getInstance();

      LoginRequest loginRequest = new LoginRequest(
          username: usrController.text.toString(),
          password: pswController.text.toString(),
          clientId: 'quickapp_spa',
          scopes: 'openid profile phone email role quickapp_api',
          grantType: 'password');

      final logger = Logger();

      final dio = Dio(); // Provide a dio instance

      final client = RestClient(dio);

      client
          .login(
              loginRequest.username,
              loginRequest.password,
              loginRequest.clientId,
              loginRequest.scopes,
              loginRequest.grantType)
          .then((res) async {
        await _prefs.setString('token', res.accessToken);
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) => MainPage()));
        Fluttertoast.showToast(
            msg: 'user Logged in ',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Color(0xffff1493),
            textColor: Colors.white,
            fontSize: 16.0);
      }).catchError((Object obj) {
        Fluttertoast.showToast(
            msg: "username or password is invalid",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Color(0xffff1493),
            textColor: Colors.white,
            fontSize: 16.0);
      });
    }
  }
}

String pswValidator(String value) {
//    String  pattern = r'^(?=.?[A-Z])(?=.?[a-z])(?=.?[0-9])(?=.?[!@#\$&*~]).{6,}$';
//    RegExp regExp = new RegExp(pattern);
  if (value.isEmpty) {
    return 'Please enter  password';
  } else {
    return null;
  }
}

String emailValidator(String value) {
//    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

//    RegExp regExp1 = new RegExp(p);

//    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
//    RegExp regExp2 = new RegExp(patttern);

  if (value.isEmpty) {
    return 'Please enter email id or phone number';
  } else {
    return null;
  }
}
