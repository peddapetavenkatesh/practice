import 'package:chat/models/Response/UserProfileResponse.dart';
import 'package:chat/screens/UserProfilePostWidget.dart';
import 'package:chat/services/apiServices/userProfileService/UserProfileApi.dart';
import 'package:chat/services/apiServices/userProfileService/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<UserProfileResponse> users = [];

  @override
  void initState() {
    super.initState();
    Provider.of<UserProvider>(context, listen: false).setListItems(
        UserProfileApi(preferences: _prefs, contextData: context));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home page'),
          centerTitle: true,
          actions: [
            IconButton(
              icon: Icon(Icons.verified_user),
              onPressed: () {},
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: _pullRefresh,
          child: ListView(
            padding: EdgeInsets.all(8.0),
            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            children: [
              Container(
                child: Column(children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    child: getList(
                        Provider.of<UserProvider>(context, listen: true).items),
                  )
                ]),
              ),
            ],
          ),
        ),
        floatingActionButton: CircleAvatar(
          radius: 30,
          backgroundColor: Color(0xff94d500),
          child: IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.black,
              ),
              onPressed: () {
                Provider.of<UserProvider>(context, listen: false)
                    .setOnEdit(null, false);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UserProfilePostWidget()),
                );
              }),
        ),
      ),
    );
  }

  Future _pullRefresh() async {
    await Future.delayed(Duration(seconds: 2));

    await Provider.of<UserProvider>(context, listen: false).setListItems(
        UserProfileApi(preferences: _prefs, contextData: context));
  }

  getList(List<UserProfileResponse> list) {
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (builder, index) => Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text('name: ${list[index].name}'),
                        Spacer(),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () {
                            Provider.of<UserProvider>(context, listen: false)
                                .setOnEdit(list[index], true);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      UserProfilePostWidget()),
                            );
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            await Provider.of<UserProvider>(context,
                                    listen: false)
                                .deleteUser(
                                    UserProfileApi(
                                        contextData: context,
                                        preferences: _prefs),
                                    list[index].userProfileId);
                          },
                        ),
                      ],
                    ),
                    Text('Email: ${list[index].email}'),
                    Text('phonenumber: ${list[index].phoneNumber}'),
                    Text('address: ${list[index].address}')
                  ],
                ),
              ),
            ));
  }
}
