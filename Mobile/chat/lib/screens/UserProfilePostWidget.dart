import 'package:chat/models/Response/UserProfileResponse.dart';
import 'package:chat/services/apiServices/userProfileService/UserProfileApi.dart';
import 'package:chat/services/apiServices/userProfileService/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProfilePostWidget extends StatefulWidget {
  UserProfilePostWidget({Key key}) : super(key: key);

  @override
  _UserProfilePostWidgetState createState() => _UserProfilePostWidgetState();
}

class _UserProfilePostWidgetState extends State<UserProfilePostWidget> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  int userProfileId = 0;

  @override
  void initState() {
    super.initState();

    var item = Provider.of<UserProvider>(context, listen: false).getItem;
    var isEdit = Provider.of<UserProvider>(context, listen: false).isEdit;
    if (isEdit) {
      nameController.text = item.name;
      emailController.text = item.email;
      phoneController.text = item.phoneNumber;
      addressController.text = item.address;
      setState(() {
        userProfileId = item.userProfileId;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var isEdit = Provider.of<UserProvider>(context, listen: true).isEdit;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(isEdit ? 'Edit User' : 'Post User'),
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              ListTile(
                title: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'name'),
                ),
              ),
              ListTile(
                title: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: emailController,
                  decoration: InputDecoration(labelText: 'email'),
                ),
              ),
              ListTile(
                title: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: phoneController,
                  decoration: InputDecoration(labelText: 'phonenumber'),
                ),
              ),
              ListTile(
                title: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: addressController,
                  decoration: InputDecoration(labelText: 'address'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () async {
                  var data = UserProfileResponse(
                      name: nameController.text,
                      email: emailController.text,
                      address: addressController.text,
                      phoneNumber: phoneController.text,
                      userProfileId: userProfileId);
                  if (isEdit) {
                    await Provider.of<UserProvider>(context, listen: false)
                        .putUser(
                            UserProfileApi(
                                preferences: _prefs, contextData: context),
                            data);
                  } else {
                    await Provider.of<UserProvider>(context, listen: false)
                        .postUser(
                            UserProfileApi(
                                preferences: _prefs, contextData: context),
                            data);
                  }
                },
                child: Container(
                  height: MediaQuery.of(context).size.width / 10,
                  width: MediaQuery.of(context).size.width * .6,
                  child: Center(
                      child: Text(
                    isEdit ? "update" : "Post",
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        color: Colors.white,
                        fontSize: 18),
                  )),
                  decoration: BoxDecoration(
                    color: Colors.blue[900],
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.black),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
