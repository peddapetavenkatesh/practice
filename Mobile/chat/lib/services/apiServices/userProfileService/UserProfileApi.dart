import 'package:chat/models/Response/UserProfileResponse.dart';
import 'package:chat/utils/ReuseLogic.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../retrofit.dart';

class UserProfileApi {
  final Future<SharedPreferences> preferences;
  final dynamic contextData;

  Future<RestClient> getRestClient() async {
    final dio = Dio();
    var _pref = await preferences;
    var accesstoken = _pref.getString('token');
    dio.options.headers["Authorization"] = ("Bearer " + accesstoken);
    final client = RestClient(dio);
    return client;
  }

  UserProfileApi({this.preferences, this.contextData});

  Future<List<UserProfileResponse>> getList() async {
    final client = await getRestClient();
    var some = client.getUserProfiles().then((res) async {
      return res;
    }).catchError((Object obj) async {
      final res = (obj as DioError).response;
      if (res.statusCode == 401) {
        ReuseLogic().logout(this.preferences, this.contextData);
      }
    });
    return some;
  }

  Future postUser(UserProfileResponse reqeust, Function fn) async {
    final client = await getRestClient();
    client.postUserProfile(reqeust).then((res) async {
      fn();
      Fluttertoast.showToast(
          msg: "user added successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Color(0xffff1493),
          textColor: Colors.white,
          fontSize: 16.0);
      return res;
    }).catchError((Object obj) async {
      final res = (obj as DioError).response;
      if (res.statusCode == 401) {
        ReuseLogic().logout(this.preferences, this.contextData);
      }
    });
  }

  Future putUser(UserProfileResponse reqeust, Function fn) async {
    final client = await getRestClient();
    client.putUserProfile(reqeust.userProfileId, reqeust).then((res) async {
      fn();
      Fluttertoast.showToast(
          msg: "user updated successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Color(0xffff1493),
          textColor: Colors.white,
          fontSize: 16.0);
      return res;
    }).catchError((Object obj) async {
      final res = (obj as DioError).response;
      if (res.statusCode == 401) {
        ReuseLogic().logout(this.preferences, this.contextData);
      }
    });
  }

  Future deleteUser(int id, Function fn) async {
    final client = await getRestClient();
    client.deleteUserProfile(id).then((res) async {
      fn();
      Fluttertoast.showToast(
          msg: "user deleted successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Color(0xffff1493),
          textColor: Colors.white,
          fontSize: 16.0);
      return res;
    }).catchError((Object obj) async {
      Fluttertoast.showToast(
          msg: "user deleted error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Color(0xffff1493),
          textColor: Colors.white,
          fontSize: 16.0);
      final res = (obj as DioError).response;
      if (res.statusCode == 401) {
        ReuseLogic().logout(this.preferences, this.contextData);
      }
    });
  }
}
