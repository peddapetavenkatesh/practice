import 'dart:async';

import 'package:chat/models/Response/UserProfileResponse.dart';
import 'package:chat/services/apiServices/userProfileService/UserProfileApi.dart';
import 'package:flutter/material.dart';

class UserProvider extends ChangeNotifier {
  List<UserProfileResponse> items = [];
  UserProfileResponse getItem;
  var isQueue = false;
  var isEdit = false;

  Future setListItems(UserProfileApi userApi) async {
    items = await userApi.getList();
    notifyListeners();
  }

  Future putUser(UserProfileApi userApi, UserProfileResponse reqeust) async {
    await userApi.putUser(reqeust, () async {
      items = await userApi.getList();
      notifyListeners();
      Navigator.pop(userApi.contextData);
    });
  }

  Future postUser(UserProfileApi userApi, UserProfileResponse reqeust) async {
    await userApi.postUser(reqeust, () async {
      items = await userApi.getList();
      notifyListeners();
      Navigator.pop(userApi.contextData);
    });
  }

  Future deleteUser(UserProfileApi userApi, int id) async {
    await userApi.deleteUser(id, () async {
      items = await userApi.getList();
      notifyListeners();
    });
  }

  void setOnEdit(UserProfileResponse reqeust, bool isEdit) {
    this.isEdit = isEdit;
    getItem = reqeust;
    notifyListeners();
  }
}
