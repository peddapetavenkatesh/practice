import 'dart:core';

import 'package:chat/models/LoginResponse.dart';
import 'package:chat/models/Response/UserProfileResponse.dart';
import 'package:dio/dio.dart';

import 'package:retrofit/retrofit.dart';

part 'retrofit.g.dart';

@RestApi(baseUrl: 'https://192.168.1.12:5001/')
abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;

  @POST("connect/token")
  @MultiPart()
  Future<LoginResponse> login(
    @Part(name: 'username') String username,
    @Part(name: 'password') String password,
    @Part(name: 'client_Id') String client_Id,
    @Part(name: 'scopes') String scopes,
    @Part(name: 'grant_type') String granttype,
  );
  @GET('api/userprofiles')
  Future<List<UserProfileResponse>> getUserProfiles();

  @POST('api/userprofiles')
  Future postUserProfile(@Body() UserProfileResponse userProfileResponse);

  @PUT('api/userprofiles/{id}')
  Future putUserProfile(
      @Path("id") int id, @Body() UserProfileResponse userProfileResponse);

  @DELETE('api/userprofiles/{id}')
  Future deleteUserProfile(@Path("id") int id);
}
