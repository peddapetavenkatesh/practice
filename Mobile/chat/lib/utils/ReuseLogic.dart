import 'package:chat/screens/LoginScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReuseLogic {
  logout(Future<SharedPreferences> preferences, context) async {
    var pref = await preferences;
    pref.clear();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
  }
}
